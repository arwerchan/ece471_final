/**
 * 
 */
package gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.filechooser.FileNameExtensionFilter;

import steganography.Encode;


/**
 * @author coff33bit
 *
 */
public class UserInterface extends JFrame {

	/**
	 * Default serial ID
	 */
	private static final long serialVersionUID = 1L;
	private File f_h;
	private File f_t;
	private File f_k;
	private JPanel contentPane;
	protected JRadioButton encode_opt;
	protected JRadioButton decode_opt;
	protected ButtonGroup btnGrp;
	protected boolean encodeFlag;
	/**
	 * Create the user interface panel
	 */
	public UserInterface() {
		// super constructor from JFrame with title
		super("Encode and Decode");
		// Set how the window should end its process
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// set window to 800x600 pixels, 4:3 ratio
		setBounds(0,0,600,450);
		// Make a JPanel to hold all of the buttons and fields
		contentPane = new JPanel();	
		// Set the layout of the panel
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		this.f_h = null;
		this.f_k = null;
		this.f_t = null;
		
		// create a label with instructions on how to use the program
		JLabel welcomeLabel = new JLabel("<HTML><center>Please enter the "
				+ "the locations of the host and source "
				+ "files.<Br> If encoding, no key file is needed</Br><center></HTML>");
		welcomeLabel.setFont(new Font("Tahoma", Font.BOLD,16));
		welcomeLabel.setBounds(50, 20, 500, 60);
		contentPane.add(welcomeLabel);
		
		// add radio buttons to select either decode or encode
		JRadioButton encode_opt = new JRadioButton("Encode");
		encode_opt.setMnemonic(KeyEvent.VK_E);
		encode_opt.setActionCommand("Encode");
		encode_opt.setBounds(200,325,100,20);
		encode_opt.addActionListener(new selectionListener());
		
		JRadioButton decode_opt = new JRadioButton("Decode");
		decode_opt.setMnemonic(KeyEvent.VK_D);
		decode_opt.setActionCommand("Decode");
		decode_opt.setBounds(300,325,100,20);
		decode_opt.addActionListener(new selectionListener());
		
		// Create a ButtonGroup to hold the radio button options
		ButtonGroup btnGrp = new ButtonGroup();
		btnGrp.add(encode_opt);
		btnGrp.add(decode_opt);
		
		// add the buttons to the content pane
		contentPane.add(encode_opt);
		contentPane.add(decode_opt);
		
		// add a field for the location of the host and source file
		JLabel hostLabel = new JLabel("Enter the location of the host file:");
		hostLabel.setBounds(50,200,300,20);
		contentPane.add(hostLabel);
		
		JLabel sourceLabel = new JLabel("Select the source file to encode:");
		sourceLabel.setBounds(50,240,300,20);
		contentPane.add(sourceLabel);
		
		JLabel keyLabel = new JLabel("Select the key file:");
		keyLabel.setBounds(50,280,300,20);
		contentPane.add(keyLabel);
		
		// add a button to execute the program once all fields are entered
		// make the new button
		JButton execute = new JButton("Execute");
		execute.setFont(new Font("Tahoma",Font.BOLD,12));
		execute.setForeground(new Color(34, 140, 34));
		execute.setBackground(Color.LIGHT_GRAY);
		execute.setBounds(315, 375, 150, 20);
		contentPane.add(execute);
		execute.addActionListener(new selectionListener());
		
		JButton exit = new JButton("Exit");
		exit.setFont(new Font("Tahoma",Font.BOLD,12));
		exit.setForeground(Color.BLACK);
		exit.setBackground(Color.LIGHT_GRAY);
		exit.setBounds(135,375,150,20);
		contentPane.add(exit);
		exit.addActionListener(new selectionListener());
				
		JButton addHost = new JButton("Select Host File");
		addHost.setFont(new Font("Tahoma",Font.BOLD,12));
		addHost.setForeground(Color.BLACK);
		addHost.setBackground(Color.LIGHT_GRAY);
		addHost.setBounds(300,200,160,20);
		contentPane.add(addHost);
		addHost.addActionListener(new selectionListener());
		
		JButton addSource = new JButton("Select Target File");
		addSource.setFont(new Font("Tahoma",Font.BOLD,12));
		addSource.setForeground(Color.BLACK);
		addSource.setBackground(Color.LIGHT_GRAY);
		addSource.setBounds(300,240,160,20);
		contentPane.add(addSource);
		addSource.addActionListener(new selectionListener());
		
		JButton addKey = new JButton("Select Key File");
		addKey.setFont(new Font("Tahoma",Font.BOLD,12));
		addKey.setForeground(Color.BLACK);
		addKey.setBackground(Color.LIGHT_GRAY);
		addKey.setBounds(300,280,160,20);
		contentPane.add(addKey);
		addKey.addActionListener(new selectionListener());
		
		this.setLocation(100, 100);
		// Make the new window visible 
		this.setVisible(true);
	}
	
	private class selectionListener implements ActionListener {
		
		@SuppressWarnings("unused")
		private JRadioButton b1 = encode_opt;
		@SuppressWarnings("unused")
		private JRadioButton b2 = decode_opt;
		
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("Execute")) {
				System.out.println("Execute pressed");
				System.out.println(encodeFlag);
				try {
					if (f_t != null && f_h != null) {
						Encode e1 = new Encode(f_t.toString(), f_h.toString());
					} else {
						if (f_t == null && f_h != null) {
							JOptionPane.showMessageDialog(null,"No target image selected", "WARNING",
														  JOptionPane.WARNING_MESSAGE);
						} else if (f_t != null && f_h == null) {
							JOptionPane.showMessageDialog(null, "No host image selected", "WARNING",
														  JOptionPane.WARNING_MESSAGE);
						} else {
							JOptionPane.showMessageDialog(null, "No host or target images selected", "WARNING",
														  JOptionPane.WARNING_MESSAGE);
						}
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			if (e.getActionCommand().equals("Select Host File")){
				System.out.println("Select Host Pressed");
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle("Select Host File");
				FileNameExtensionFilter f_jpg = new FileNameExtensionFilter("JPEG Files", "jpg", 
																			"jpeg");
				
				FileNameExtensionFilter f_bmp = new FileNameExtensionFilter("Bitmap Files", "bmp");
				
				fc.addChoosableFileFilter(f_jpg);
				fc.addChoosableFileFilter(f_bmp);
				
				fc.showOpenDialog(contentPane);
				f_h = fc.getSelectedFile();
				//TODO print for debug confirmation
				System.out.println(f_h);
			}
			if (e.getActionCommand().equals("Select Target File")) {
				System.out.println("Select Target Pressed");
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle("Select Target File");
				FileNameExtensionFilter f_jpg = new FileNameExtensionFilter("JPEG Files", "jpg", 
																			"jpeg");

				FileNameExtensionFilter f_bmp = new FileNameExtensionFilter("Bitmap Files", "bmp");
				
				fc.addChoosableFileFilter(f_jpg);
				fc.addChoosableFileFilter(f_bmp);
				
				fc.showOpenDialog(contentPane);
				f_t = fc.getSelectedFile();
				//TODO print for debug confirmation
				System.out.println(f_t);
			}
			if (e.getActionCommand().equals("Select Key File")) {
				System.out.println("Select Key Pressed");
				JFileChooser fc = new JFileChooser();
				
				FileNameExtensionFilter f_txt = new FileNameExtensionFilter("Text Files", "txt");
				
				fc.addChoosableFileFilter(f_txt);
				
				fc.showOpenDialog(contentPane);
				f_k = fc.getSelectedFile();
				System.out.println(f_k);
			}
			if (e.getActionCommand().equals(("Exit"))) {
				System.out.println("exiting");
				dispose();
			}
			if (e.getSource() instanceof JRadioButton) {
				JRadioButton j = (JRadioButton) e.getSource();
				if (j.getActionCommand().equals("Encode")) {
					encodeFlag = true;
				}
				if (j.getActionCommand().equals("Decode")) {
					encodeFlag = false;
				}
			}
		}
	}
}
