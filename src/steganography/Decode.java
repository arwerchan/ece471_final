/**
 * 
 */
package steganography;

/**
 * @author ltdouthit, arwerchan
 *
 */
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class Decode {
	
	private String host_image_location;
	private BufferedImage host_image;
	private boolean pass_fail;
	
	public Decode(String Cypher) throws IOException{
		System.out.println(Cypher);
		host_image_location = Cypher;
		System.out.println("Reading \n");
		read_in_host(host_image_location);
		System.out.println("Read \n");
		decode_image();
	}
	private void decode_image() throws IOException {
		System.out.println("In decode_image \n");
		

		//Get Info About Images
		
		byte[] imageInByte_host;
		@SuppressWarnings("unused")
		byte[] imageInByte_target;
		//Create Variable for holding image bytes 
		ByteArrayOutputStream baos_target = new ByteArrayOutputStream();
		ByteArrayOutputStream baos_host   = new ByteArrayOutputStream();
		ImageIO.write(host_image,   "bmp", baos_host);
		
		
		baos_host.flush();
		baos_target.flush();
		imageInByte_host = baos_host.toByteArray();
		imageInByte_target = baos_target.toByteArray();
		baos_host.close();
		baos_target.close();
		
		InputStream in_host = new ByteArrayInputStream(imageInByte_host);
		
		BufferedImage buffedImage_host = ImageIO.read(in_host);
		//Get Info About Images
		int height_host = buffedImage_host.getHeight();
		int width_host  = buffedImage_host.getWidth();

		Color temp = new Color(buffedImage_host.getRGB(0,0));
		int h_r = temp.getRed();
		int h_g = temp.getGreen();
		int h_b = temp.getBlue();
		int h = (h_r<<16) + (h_g<<8)+ h_b; 
		
		
		
		Color temp1 = new Color(buffedImage_host.getRGB(1,0));
		int w_r = temp1.getRed();
		int w_g = temp1.getGreen();
		int w_b = temp1.getBlue();
		int w = (w_r<<16) + (w_g<<8)+ w_b; 
		System.out.println("Height: " + h +"\nWidth: "+ w );
		
		
		
		int color_t = buffedImage_host.getRGB((2)%width_host,(2)%height_host);
		int m = 0;
		for(int i = 1; i < height_host; i++){
			for(int j = 1; j < width_host; j++){
				color_t = buffedImage_host.getRGB(((j*2))%width_host, ((i*2))%height_host);
				if(i < h & j<w)
				{
					buffedImage_host.setRGB((j)%w,(i)%h, color_t );
				}
				else {
					//color_t = buffedImage_host.getRGB(j,i);
					//buffedImage_host.setRGB((j)%width_host,(i)%height_host, color_t);
				}
			m = m+1;
		
			}
		}
		 System.out.println("Writing \n");
			ImageIO.write(buffedImage_host, "bmp", new File(
					"decoded.bmp"));
			
			return;
		
	}
	public boolean read_in_host(String image_loc){
		try {
			host_image = ImageIO.read(new File(image_loc));
			pass_fail = true;
		} catch (IOException e) {
			System.out.println("Could not read given file path to host image. \n");
			e.printStackTrace();
			pass_fail = false;
		}
		return pass_fail;
	}
	
}