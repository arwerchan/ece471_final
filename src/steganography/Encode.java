/**
 * 
 */
package steganography;

/**
 * @author ltdouthit, arwerchan
 *
 */
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;

public class Encode {
	
	private String host_image_location;
	private String tgt_image_location;
	private BufferedImage host_image;
	private BufferedImage target_image;
	private boolean pass_fail;
	
	public Encode(String target, String location) throws IOException{
		System.out.println(target);
		System.out.println(location);
		host_image_location = target;
		tgt_image_location  = location;
		read_in_host(host_image_location);
		read_in_tgt(tgt_image_location);
		encode_image();
	}
	
	public boolean read_in_host(String image_loc){
		try {
			host_image = ImageIO.read(new File(image_loc));
			pass_fail = true;
		} catch (IOException e) {
			System.out.println("Could not read given file path to host image. \n");
			e.printStackTrace();
			pass_fail = false;
		}
		return pass_fail;
	}
	
	public boolean read_in_tgt(String target_loc){
		try {
			target_image = ImageIO.read(new File(target_loc));
			pass_fail = true;
		} catch (IOException e) {
			System.out.println("Could not read given file path to tgt_image. \n");
			e.printStackTrace();
			pass_fail = false;
		}
		return pass_fail;
	}
	
	public boolean encode_image() throws IOException{
		
		System.out.println("In encode_image \n");
		
		byte[] imageInByte_host;
		byte[] imageInByte_target;
		//Create Variable for holding image bytes 
		ByteArrayOutputStream baos_target = new ByteArrayOutputStream();
		ByteArrayOutputStream baos_host   = new ByteArrayOutputStream();
		ImageIO.write(host_image,   "bmp", baos_host);
		ImageIO.write(target_image, "bmp", baos_target);
		
		baos_host.flush();
		baos_target.flush();
		imageInByte_host = baos_host.toByteArray();
		imageInByte_target = baos_target.toByteArray();
		baos_host.close();
		baos_target.close();
		
		InputStream in_host = new ByteArrayInputStream(imageInByte_host);
		InputStream in_target = new ByteArrayInputStream(imageInByte_target);
		
		BufferedImage buffedImage_host = ImageIO.read(in_host);
		BufferedImage buffedImage_target = ImageIO.read(in_target);
		//Get Info About Images
		int height_host = buffedImage_host.getHeight();
		int width_host  = buffedImage_host.getWidth();
		
		int height_target = buffedImage_target.getHeight();
		int width_target  = buffedImage_target.getWidth();
		
		
		System.out.println(height_target);
		System.out.println(Integer.toBinaryString(height_target) + "\n");
		
		
		int h_r = (height_target>>16) & 0xFF;
		System.out.println(Integer.toBinaryString(h_r) + "\n");
		System.out.println("R:" + h_r);
		
		int h_g = (height_target>>8)  & 0xFF;
		System.out.println(Integer.toBinaryString(h_g) + "\n");
		System.out.println("G:"+h_g);

		int h_b = (height_target)     & 0xFF;
		System.out.println(Integer.toBinaryString(h_b) + "\n");
		System.out.println("B: "+h_b);
		
		Color temp_h = new Color(h_r, h_g, h_b);
		buffedImage_host.setRGB(0, 0, temp_h.getRGB());
		
		System.out.println("Width: " +width_target);
		System.out.println(Integer.toBinaryString(width_target) + "\n");
		int w_r = (width_target >> 16) & 0xFF;
		System.out.println(Integer.toBinaryString(w_r) + "\n");
		System.out.println("R:" + w_r);
		int w_g = (width_target>>8)  & 0xFF;
		System.out.println(Integer.toBinaryString(w_g) + "\n");
		System.out.println("B:" + w_g);
		int w_b = (width_target) & 0xFF;
		System.out.println(Integer.toBinaryString(w_b) + "\n");
		System.out.println("G:" + w_b);
		
		Color temp_w = new Color(w_r, w_g, w_b);
		

		
		int color_t = buffedImage_target.getRGB(2, 2);
		//Start Encoding Process.
		int m = 0;
		for(int i = 1; i < height_host; i++){
			for(int j = 1; j < width_host; j++){
				
				
				//How You put image into other image 
				//Copy this into decodes 
				if(i < height_target & j<width_target)
				{
					color_t = buffedImage_target.getRGB(j, i);
					buffedImage_host.setRGB(((j*2))%width_host,((i*2))%height_host, color_t);
				}
				else {
					color_t = buffedImage_host.getRGB(j,i);
					buffedImage_host.setRGB((j)%width_host,(i)%height_host, color_t);
				}
				m=m+1;
		
			}
		}
		buffedImage_host.setRGB(0, 0, temp_h.getRGB());
		buffedImage_host.setRGB(1, 0, temp_w.getRGB());
	    System.out.println("Writing \n");
		
		ImageIO.write(buffedImage_host, "bmp", new File(
				"darksouls.bmp"));
		
		return pass_fail;
	}
	
	public byte[] extractBytes (BufferedImage Image) throws IOException {

		 // get DataBufferBytes from Raster
		 WritableRaster raster = Image .getRaster();
		 DataBufferByte data   = (DataBufferByte) raster.getDataBuffer();

		 return ( data.getData() );
		}
	
}